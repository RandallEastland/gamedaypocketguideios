//
//  ViewController.swift
//  GamedayPocketGuide
//
//  Created by Randall Eastland on 9/2/17.
//  Copyright © 2017 Randall Eastland. All rights reserved.
//

import UIKit
import Alamofire

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getConfigString()
    }

    func getConfigString() {
        
        let url = Constants.Urls.baseUrl + "tab-list.json"
        
        Alamofire.request(url, method: .get)
            .responseJSON { response in
                
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        self.parseJsonResponse(response: data)
                    }
                    
                case .failure(_):
                    
                    print("Error message:\(String(describing: response.result.error))")
                    break
                
                }
            }
    }
    
    func parseJsonResponse (response: Any) {
        print("JSON:\(response)")
    }
}

