//
//  Tab.swift
//  GamedayPocketGuide
//
//  Created by Randall Eastland on 9/2/17.
//  Copyright © 2017 Randall Eastland. All rights reserved.
//

import UIKit

class Tab {

    var name: String
    var title: String
    var color: String
    var sections: [Section]

    init (name: String, title: String, color: String, sections: [Section]) {
        self.name = name
        self.title = title
        self.color = color
        self.sections = sections
    }
}
