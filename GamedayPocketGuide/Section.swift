//
//  Section.swift
//  GamedayPocketGuide
//
//  Created by Randall Eastland on 9/2/17.
//  Copyright © 2017 Randall Eastland. All rights reserved.
//

import UIKit

class Section {
    
    var type: String
    var text: String
    var imageName: String
    
    init (type: String, text: String, imageName: String) {
        self.type = type
        self.text = text
        self.imageName = imageName
    }
}
